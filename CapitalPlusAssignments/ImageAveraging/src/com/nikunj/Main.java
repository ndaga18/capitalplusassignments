package com.nikunj;

import java.util.Scanner;

public class Main {
    private static int MAX_PIXEL_VALUE = 255;
    private static int MIN_PIXEL_VALUE = 0;

    /***
     * We are going to add 2 matrices and
     * assume that grayscale images are added.
     */
    private static void inputOperations() {
        int i = 0, j = 0, row = 0, col = 0;

        Scanner scan = new Scanner(System.in);

        System.out.print("Enter no of rows :");
        row = scan.nextInt();
        System.out.print("Enter no of columns :");
        col = scan.nextInt();

        int inputImage1[][] = new int[row][col];
        int inputImage2[][] = new int[row][col];

        System.out.print("Enter Matrix 1 Elements with values between " + MIN_PIXEL_VALUE + " and "+
                MAX_PIXEL_VALUE + ":");
        for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
                inputImage1[i][j] = scan.nextInt();
            }
        }

        System.out.print("Enter Matrix 2 Elements with values between " + MIN_PIXEL_VALUE + " and "+
                MAX_PIXEL_VALUE + ":");
        for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
                inputImage2[i][j] = scan.nextInt();
            }
        }

        averageAndNormalise(inputImage1, inputImage2, row, col);

    }

    private static void averageAndNormalise(int inputImage1[][], int inputImage2[][], int row, int col) {
        int i, j;
        int array[] = new int[row * col];
        int max = array[0];

        for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
                array[i * col + j] = (inputImage1[i][j] + inputImage2[i][j]) / 2;
                if (max < array[ i* col +j]) max = array[i * col +j];
            }
        }

        for (i = 0; i < array.length; i++) {
            if (max > MIN_PIXEL_VALUE) array[i] = (array[i] * MAX_PIXEL_VALUE) / max;
        }

        outputOperations(row, col, array);
    }

    private static void outputOperations(int row, int col, int array[]) {
        int outputImage[][] = new int[row][col];
        int i, j;

        for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
                outputImage[i][j] = array[i * col + j];
            }
        }

        System.out.print("The Output Image is :\n");

        for (i = 0; i < row; i++) {
            for (j = 0; j < col; j++) {
                System.out.print(outputImage[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void performOperations() {
        inputOperations();
    }

    public static void main(String args[]) {
        performOperations();
    }

}
